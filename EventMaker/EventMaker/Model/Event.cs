﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventMaker.Model
{
	public class Event
	{
		private static int IdCounter = 0;

		public int Id { get; set; }
		public string Name { get; set; }
		public string Place { get; set; }
		public DateTimeOffset Start { get; set; }
		public DateTimeOffset End { get; set; }

		public Event(string name, string place, DateTimeOffset start, DateTimeOffset end)
		{
			IdCounter++;
			Id = IdCounter;

			Name = name;
			Place = place;
			Start = start;
			End = end;
		}
	}
}
