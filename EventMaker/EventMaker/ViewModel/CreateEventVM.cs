﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using EventMaker.Common;
using EventMaker.Model;

namespace EventMaker.ViewModel
{
	internal class CreateEventVM : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private string _error;

		public string Title { get; set; }

		public string Place { get; set; }

		public string Feedback
		{
			set
			{
				_error = value;
				OnPropertyChanged("Feedback");
			}
			get => _error;
		}

		public void OnPropertyChanged(string name)
		{
			var handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(name));
		}

		public DateTimeOffset DateStart { get; set; }

		public DateTimeOffset DateEnd { get; set; }

		public ICommand AddCommand { get; set; }
		public TimeSpan TimeStart { get; set; }
		public TimeSpan TimeSlut { get; set; }


		public EventCatalogSingleton tempCatalogSingle = EventCatalogSingleton.Instance;

		//public ICommand AddEvent { get; }

		public bool Validation()
		{
			bool tempvali = false;
			Feedback = "";

			int counter = 0;

			// Check input ikke er tomme
			if (Title == "" || Place == "")
			{
				Feedback += "Navn eller Sted er tom. ";
			}
			else
				counter++;

			// Check datoer ikke er ens
			if (DateStart.Year == DateEnd.Year && DateStart.Day == DateEnd.Day && TimeStart.Hours == TimeSlut.Hours &&
			    TimeStart.Minutes == TimeSlut.Minutes)
			{
				Feedback += "Start og slut tidspunkt/dato må ikke være ens. ";
			}
			else
				counter++;
			
			// checker om der er nogle værdier, i start datoen, som er større end slut datoen
			if (DateStart.DateTime.Date > DateEnd.DateTime.Date)
			{
				Feedback += "Datoerne kan ikke lade sig gøre. ";
			}
			else
				counter++;

			// endTime er før startTime
			if (TimeStart > TimeSlut)
			{
				Feedback += "tidspunkterne kan ikke lad sig gøre. ";
			}
			else
				counter++;

			if (counter == 4)
			{
				tempvali = true;
				Feedback = "Dit event er oprretet GZ";
			}

			return tempvali;
		}

		public CreateEventVM()
		{
			Title = "";
			Place = "";
			tempCatalogSingle = EventCatalogSingleton.Instance;
			AddCommand = new RelayCommand(CreateEvent);
		}

		// string title, string place, DateTime start, DateTime end
		public void CreateEvent()
		{
			if (Validation()) EventCatalogSingleton.Instance.Events.Add(new Event(Title, Place, DateStart, DateEnd));
			// check if there is input
			// add to the list
		}

		public DateTime Dateconverter()
		{
			var tempdatetime = new DateTime();

			return tempdatetime;
		}
	}
}